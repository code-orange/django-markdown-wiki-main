import os

from django.conf import settings


def get_content_packages():
    return next(os.walk(settings.WIKI_CONTENT_DIR))[1]


def get_languages(content_package=None, content_file=None):
    languages = list()

    for content_package_local in get_content_packages():
        languages += get_languages(content_package_local)

    if content_package is not None:
        content_folder = os.path.join(
            settings.WIKI_CONTENT_DIR, content_package, "content"
        )
        for lang_code in next(os.walk(content_folder))[1]:
            lang_code = lang_code.lower()
            if lang_code not in languages:
                if content_file is None:
                    languages.append(lang_code)
                else:
                    if os.path.isfile(
                        os.path.join(content_folder, lang_code, content_file)
                    ):
                        languages.append(lang_code)

    return languages
